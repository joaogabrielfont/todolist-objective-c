//
//  NewToDoViewController.h
//  ToDoList
//
//  Created by Monitora on 15/12/16.
//  Copyright © 2016 Monitora. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "Todo2.h"

@protocol AddNewTodoDelegate <NSObject>
@required
-(void)didAddNewTodo:(Todo2 *)newTodo;
@end

@interface NewToDoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *field;
@property (strong, nonatomic) id<AddNewTodoDelegate> delegate;
@end
