 //
//  ViewController.m
//  ToDoList
//
//  Created by Monitora on 15/12/16.
//  Copyright © 2016 Monitora. All rights reserved.
//

#import "ViewController.h"
#import "Todo2.h"
#import "SimpleTableCellTableViewCell.h"

@interface ViewController (){
    NSMutableArray *toDoList;
    NSMutableArray *deleteList;
}


@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [_tableView setEditing:YES animated:YES];
    toDoList = [[NSMutableArray alloc] init];
    deleteList = [[NSMutableArray alloc] init];
    [self.tableView registerNib:[UINib nibWithNibName:@"SimpleTableCell" bundle:nil] forCellReuseIdentifier:@"SimpleTableCell"];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

}

#pragma MARK - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (toDoList.count > 0){
        return toDoList.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SimpleTableCellTableViewCell *cell = (SimpleTableCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SimpleTableCell" forIndexPath:indexPath];
    
    Todo2 *todo = [toDoList objectAtIndex:indexPath.row];
    cell.tintColor = [UIColor blueColor];
    cell.nameLabel.text = todo.task;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.f;
}

#pragma MARK - UITableViewDelegate

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 3;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [deleteList addObject:toDoList[indexPath.row]];
    [toDoList[indexPath.row] setIsDone:YES];
}

-(void)tableView:(UITableView *)tableView didDeSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [deleteList removeObject:toDoList[indexPath.row]];
    [toDoList[indexPath.row] setIsDone:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma MARK AddNew Delegate
- (void)didAddNewTodo:(Todo2 *)newTodo {
    [toDoList addObject:newTodo];
    [self.tableView reloadData];
}

- (IBAction)addNewTodoTap:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NewToDoViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"NewToDoViewController"];
    vc.delegate = self;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)deleteButtonAction:(UIButton*)sender {
        if (deleteList.count){
        for (NSString *str in deleteList) {
            [toDoList removeObject:str];
        }
        [deleteList removeAllObjects];
        [self.tableView reloadData];
    }
}


@end



