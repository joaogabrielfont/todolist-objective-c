//
//  main.m
//  ToDoList
//
//  Created by Monitora on 15/12/16.
//  Copyright © 2016 Monitora. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
