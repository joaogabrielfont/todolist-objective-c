//
//  NewToDoViewController.m
//  ToDoList
//
//  Created by Monitora on 15/12/16.
//  Copyright © 2016 Monitora. All rights reserved.
//

#import "NewToDoViewController.h"

@interface NewToDoViewController ()

@end

@implementation NewToDoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)button:(id)sender {
    Todo2 *newTodo = [[Todo2 alloc]init];
    newTodo.task = self.field.text;
    
    
    // Trigger delegate
    [_delegate didAddNewTodo:newTodo];
    
    // Pop view controller
    [self.navigationController popViewControllerAnimated:YES];
}



@end
