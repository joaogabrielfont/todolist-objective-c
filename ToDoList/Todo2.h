//
//  Todo2.h
//  ToDoList
//
//  Created by Monitora on 16/12/16.
//  Copyright © 2016 Monitora. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Todo2 : NSObject
@property (strong, nonatomic) NSString  *task;
@property (nonatomic) BOOL isDone;
@end
