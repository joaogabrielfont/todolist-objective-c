//
//  ViewController.h
//  ToDoList
//
//  Created by Monitora on 15/12/16.
//  Copyright © 2016 Monitora. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewToDoViewController.h"


@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, AddNewTodoDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addNewTodo;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *deleteTask;



- (IBAction)addNewTodoTap:(id)sender;
- (IBAction)deleteButtonAction:(id)sender;


@end


